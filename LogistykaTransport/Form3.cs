﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LogistykaTransport
{
    public partial class Form3 : Form
    {
        private int odbiorcy_count;
        private int dostawcy_count;
        private int[,] matrix;
        private string suma;
        public Form3(int[,] matrix, string suma, int dostawcy_count, int odbiorcy_count)
        {
            InitializeComponent();
            this.odbiorcy_count = odbiorcy_count;
            this.dostawcy_count = dostawcy_count;
            this.matrix = matrix;
            this.suma = suma;

            // ResultBox;
            var image = new Bitmap(this.ResultBox.Width, this.ResultBox.Height);
            var font = new Font("TimesNewRoman", 40, FontStyle.Regular, GraphicsUnit.Pixel);
     
            var graphic = Graphics.FromImage(image);

            for( int i = 0; i<dostawcy_count; i++)
                for(int j = 0; j<odbiorcy_count; j++)
                {
                    graphic.DrawString(matrix[0,i*dostawcy_count+j].ToString(), font, Brushes.MediumAquamarine, new Point(j*60, i*60));
                }

            graphic.DrawString("Suma = "+suma, font, Brushes.Black, new Point(400,300));
            //   graphic.DrawString("xDDD", font, Brushes.Black, new Point(10, 10));
            this.ResultBox.Image = image;
        }
    }
}
