﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Linq;

namespace LogistykaTransport
{
    public partial class Form1 : Form
    {
        private int dostawcy_count = 0;
        private int odbiorcy_count = 0;
        private List<TextBox> TextBoxList = new List<TextBox>();
        private int[,] DataArray;
        private int[] PopytArray;
        private int[] PodazArray;
        private int total_popyt;
        private int total_podaz;
        private int[,] matrix2;
        private string suma;

        private void DisplayResults()
        {
            var frm = new Form3(matrix2 , suma, dostawcy_count, odbiorcy_count);
            frm.Location = this.Location;
            frm.StartPosition = FormStartPosition.Manual;
            frm.FormClosing += delegate { this.Show(); };
            frm.Show();
            this.Hide();
        }
        private void Test_Console()
        {
            for (int i = 0; i < dostawcy_count; i++)
            {
                System.Console.WriteLine();
                for (int j = 0; j < odbiorcy_count; j++)
                {
                    System.Console.Write(DataArray[i, j]);
                }
            }
            System.Console.WriteLine();
            Console.WriteLine("Popyt: ");
            foreach (int tmp in PopytArray)
            {
                Console.Write(tmp + " ");
            }
            System.Console.WriteLine("\nPodaz: ");
            foreach (int tmp in PodazArray)
            {
                Console.Write(tmp + " ");
            }
            System.Console.WriteLine();
        }

        private void TextBoxt_to_table()
        {
            DataArray = new int[dostawcy_count, odbiorcy_count];

            for (int i = 0; i < dostawcy_count; i++)
            {
                for (int j = 0; j < odbiorcy_count; j++)
                {
                    DataArray[i, j] = Int32.Parse(TextBoxList[((i * odbiorcy_count) + j)].Text);
                }
            }
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void flowLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void AddDostawca_Click(object sender, EventArgs e)
        {
            if (dostawcy_count >= 10)
            {
                MessageBox.Show("za dużo dostawców jełopie");
            }
            else
            {
                Label multi1 = new Label();
                multi1.Text = "dostawca " + (dostawcy_count + 1);
                multi1.Height = 26;
                flowLayoutPanel4.Controls.Add(multi1);

                dostawcy_count++;

                flowLayoutPanel1.Controls.Clear();
                TextBoxList.Clear();
                flowLayoutPanel1.Width = 60 * odbiorcy_count;
                flowLayoutPanel1.Height = 200 * dostawcy_count;
                for (int i = 0; i < dostawcy_count; i++)
                    for (int j = 0; j < odbiorcy_count; j++)
                    {
                        TextBox textBox = new TextBox();
                        textBox.Text = "0";
                        textBox.Width = 50;
                        TextBoxList.Add(textBox);
                        flowLayoutPanel1.Controls.Add(textBox);
                    }
            }
        }

        private void AddOdbiorca_Click(object sender, EventArgs e)
        {
            if (odbiorcy_count >= 8)
            {
                MessageBox.Show("za dużo odbiorców jełopie");
            }
            else
            {

                Label multi1 = new Label();
                multi1.Text = "odb " + (odbiorcy_count + 1);
                multi1.Width = 50;
                flowLayoutPanel3.Controls.Add(multi1);

                odbiorcy_count++;

                flowLayoutPanel1.Controls.Clear();
                TextBoxList.Clear();
                flowLayoutPanel1.Width = 60 * odbiorcy_count;
                flowLayoutPanel1.Height = 200 * dostawcy_count;
                for (int i = 0; i < dostawcy_count; i++)
                    for (int j = 0; j < odbiorcy_count; j++)
                    {
                        TextBox textBox = new TextBox();
                        textBox.Text = "0";
                        textBox.Width = 50;
                        TextBoxList.Add(textBox);
                        flowLayoutPanel1.Controls.Add(textBox);
                    }
            }
        }

        private void flowLayoutPanel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void flowLayoutPanel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void AddCeny_Click(object sender, EventArgs e)
        {
            PodazArray = new int[dostawcy_count];
            PopytArray = new int[odbiorcy_count];
            var frm = new Form2(dostawcy_count, odbiorcy_count, ref PopytArray, ref PodazArray);
            frm.Location = this.Location;
            frm.StartPosition = FormStartPosition.Manual;
            frm.FormClosing += delegate { this.Show(); };
            frm.Show();
            this.Hide();
        }

        private void DzialajKurwa(int[] PopytArray, int[] PodazArray, int[,] DataArray)
        {
            string popyt = "[";
            popyt += string.Join(",", PopytArray);
            popyt += "]";
            Console.Write(popyt, " ");

            string podaz = "[";
            podaz += string.Join(",", PodazArray);
            podaz += "]";
            Console.Write(podaz, " ");

            string costMatrix = "[";
            //Console.WriteLine(DataArray.GetLength(0));

            for (int i = 0; i < DataArray.GetLength(0); i++)
            {
                costMatrix += "[";
                for (int j = 0; j < DataArray.GetLength(1); j++)
                {
                    costMatrix += DataArray[i, j] + ",";
                }
                costMatrix += "],";
            }
            costMatrix += "]";
            Console.WriteLine(costMatrix);


            bool Laptop = true;
            string python = "";
            string myPythonApp = "";
            if (Laptop)
            {
                // full path of python interpreter 
                python = @"C:\Users\Aku\venv\logistyka\Scripts\python.exe";

                // python app to call 
                myPythonApp = @"C:\Users\Aku\Documents\logistyka_1\Logistyka_Transport\LogistykaTransport\sum.py";
            }
            else
            {
                python = "python";
                myPythonApp = @"C:\Users\admin\PycharmProjects\logistyka\proj1\tworzenie_macierzy.py";

            }
            
            // Create new process start info 
            ProcessStartInfo myProcessStartInfo = new ProcessStartInfo(python);

            // make sure we can read the output from stdout 
            myProcessStartInfo.UseShellExecute = false;
            myProcessStartInfo.RedirectStandardOutput = true;

            // start python app with 3 arguments  
            // 1st arguments is pointer to itself,  
            // 2nd and 3rd are actual arguments we want to send 



            myProcessStartInfo.Arguments = myPythonApp + " " + podaz + " " + popyt + " " + costMatrix;

            Process myProcess = new Process();
            // assign start information to the process 
            myProcess.StartInfo = myProcessStartInfo;

            Console.WriteLine("Calling Python script with arguments\n {0}",myProcessStartInfo.Arguments);
            // start the process 
            myProcess.Start();

            // Read the standard output of the app we called.  
            // in order to avoid deadlock we will read output first 
            // and then wait for process terminate: 
            StreamReader myStreamReader = myProcess.StandardOutput;
            suma = myStreamReader.ReadLine();
            string myString2 = myStreamReader.ReadLine();

            // string t = "{  { 1, 3, 23 } ,  { 5, 7, 9 } ,  { 44, 2, 3 }  }";

            /*    var cleanedRows = Regex.Split(myString2, @"}\s*,\s*{")
                                        .Select(r => r.Replace("[", "").Replace("]", "").Replace(".","").Trim())
                                        .ToList();

                var matrix = new int[cleanedRows.Count][];
                for (var i = 0; i < cleanedRows.Count; i++)
                {
                    var data = cleanedRows.ElementAt(i).Split(',');
                    matrix[i] = data.Select(c => int.Parse(c.Trim())).ToArray();
                }*/
            matrix2 = CreateMatrix(myString2);

            /*if you need to read multiple lines, you might use: 
                string myString = myStreamReader.ReadToEnd() */

            // wait exit signal from the app we called and then close it. 
            myProcess.WaitForExit();
            myProcess.Close();

            // write the output we got from python app 
            Console.WriteLine("Value received from script: " + suma + "\n\n");
            Console.WriteLine("Value received from script: " + myString2 + "\n\n");
            Console.WriteLine();
        }

        public int[,] CreateMatrix(string s)
        {
            List<string> cleanedRows = Regex.Split(s, @"}\s*,\s*{")
                                            .Select(r => r.Replace("[", "").Replace("]", "").Replace(".", "").Trim())
                                            .ToList();

            int[] columnsSize = cleanedRows.Select(x => x.Split(',').Length)
                                           .Distinct()
                                           .ToArray();

            if (columnsSize.Length != 1)
                throw new Exception("All columns must have the same size");

            int[,] matrix = new int[cleanedRows.Count, columnsSize[0]];
            string[] data;

            for (int i = 0; i < cleanedRows.Count; i++)
            {
                data = cleanedRows[i].Split(',');
                for (int j = 0; j < columnsSize[0]; j++)
                {
                    matrix[i, j] = int.Parse(data[j].Trim());
                }
            }

            return matrix;
        }

        private void Calculate_Click(object sender, EventArgs e)
        {

            total_podaz = 0;
            total_popyt = 0;
            //sprawdzanie popytu i podazy
            foreach (int tmp in PopytArray)
            {
                total_popyt += tmp;
            }
            foreach (int tmp in PodazArray)
            {
                total_podaz += tmp;
            }

            TextBoxt_to_table();
            if (total_podaz == total_popyt)
            {
                //MessageBox.Show("liczymy");
                DzialajKurwa(PopytArray, PodazArray, DataArray);

                //Testowe
                /*matrix = new int[,]
                {
                   {1,2,3 },
                   {4,5,6 }
                };*/
                DisplayResults();
            }
            else
            {
                MessageBox.Show("dodanie wirtualnego dostawcy");
                int[,] TmpDataArray = new int[dostawcy_count + 1, odbiorcy_count];

                for (int i = 0; i < dostawcy_count; i++)
                    for (int j = 0; j < odbiorcy_count; j++)
                        TmpDataArray[i, j] = DataArray[i, j];

                // DataArray = new int[dostawcy_count + 1, odbiorcy_count];
                int[] TmpPodazArray = new int[dostawcy_count + 1];
                for (int i = 0; i < dostawcy_count; i++)
                    TmpPodazArray[i] = PodazArray[i];
                TmpPodazArray[dostawcy_count] = total_podaz - total_popyt;

                dostawcy_count++;
                DataArray = TmpDataArray;
                DzialajKurwa(PopytArray, PodazArray, DataArray);
                DisplayResults();
            }
            //Test_Console();

            //display results (matrix and sum) 
        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
